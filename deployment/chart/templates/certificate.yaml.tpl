---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: "{{ .Release.Name }}-api-gateway-server"
  labels:
    name: "envvars-api"
spec:
  commonName: "envvars-api.{{ .Release.Namespace }}.svc"
  dnsNames:
    - "envvars-api.{{ .Release.Namespace }}.svc"
    - "envvars-api.{{ .Release.Namespace }}.svc.{{ .Values.certificates.internalClusterDomain }}"
  secretName: "{{ .Release.Name }}-api-gateway-server"
  usages:
    - server auth
  duration: "504h" # 21d
  privateKey:
    algorithm: ECDSA
    size: 256
  issuerRef: {{ .Values.certificates.apiGatewayCa | toYaml | nindent 4 }}
