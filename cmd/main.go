package main

import (
	"context"
	"fmt"
	"os"

	"github.com/getkin/kin-openapi/openapi3filter"
	prom "github.com/labstack/echo-contrib/echoprometheus"
	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	oapiechomiddleware "github.com/oapi-codegen/echo-middleware"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.wikimedia.org/repos/cloud/toolforge/envvars-api/gen"
	"gitlab.wikimedia.org/repos/cloud/toolforge/envvars-api/internal"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	viper.AutomaticEnv()
	viper.AllowEmptyEnv(true)
	viper.SetDefault("debug", false)
	viper.SetDefault("port", 8000)
	viper.SetDefault("address", "127.0.0.1")
	viper.SetDefault("out_of_k8s_run", false)
	viper.SetDefault("kubeconfig", fmt.Sprintf("%s/.kube/config", os.Getenv("HOME")))

	if viper.GetBool("debug") {
		log.SetLevel(log.DebugLevel)
		log.Debug("Starting in DEBUG mode")
	}
	port := viper.GetInt("port")
	address := viper.GetString("address")
	outOfK8sRun := viper.GetBool("out_of_k8s_run")
	kubeconfig := viper.GetString("kubeconfig")
	// Used for the health check
	myNamespace := viper.GetString("my_namespace")

	e := echo.New()
	e.HideBanner = true
	addMiddleware(e)

	api := getApi(outOfK8sRun, kubeconfig, myNamespace)
	gen.RegisterHandlersWithBaseURL(e, api, "")

	log.Infof("Using config: %v", api.Config)
	log.Info("Registered routes:")
	for _, route := range e.Routes() {
		log.Infof("%v", *route)
	}

	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%d", address, port)))

}

func getApi(outOfK8sRun bool, kubeconfig string, myNamespace string) *internal.EnvvarsApi {
	var k8sConfig *rest.Config
	var err error
	if outOfK8sRun {
		// use the current context in kubeconfig
		k8sConfig, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
		if err != nil {
			panic(err.Error())
		}
	} else {
		k8sConfig, err = rest.InClusterConfig()
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(k8sConfig)
	if err != nil {
		log.Fatalln(err.Error())
	}

	return &internal.EnvvarsApi{
		Clients: internal.Clients{
			K8s: clientset,
		},
		Config: internal.Config{
			MyNamespace: myNamespace,
		},
		MetricsHandler: prom.NewHandler(),
	}
}

func addMiddleware(router *echo.Echo) {
	swagger, err := gen.GetSwagger()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error loading swagger spec\n: %s", err)
		os.Exit(1)
	}

	// Log all requests
	router.Use(echomiddleware.Logger())

	// authentication
	authValidator := oapiechomiddleware.OapiRequestValidatorWithOptions(
		swagger,
		&oapiechomiddleware.Options{
			Options: openapi3filter.Options{
				AuthenticationFunc: func(ctx context.Context, input *openapi3filter.AuthenticationInput) error {
					log.Debugf("Authenticating for %s", input.RequestValidationInput.Request.URL.RequestURI())
					_, err := internal.GetUserFromRequest(input.RequestValidationInput.Request)
					if err != nil {
						return err
					}
					return nil
				},
			},
		},
	)
	router.Use(authValidator)
	// Set the user on the context for the requests if there's any, "" if there's none
	// This is needed as the above authenticator does not have access to call next with a new context
	router.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			uCtx := internal.UserContext{
				Context: c,
				User:    "",
			}
			user, err := internal.GetUserFromRequest(c.Request())
			if err == nil {
				uCtx.User = user
			}

			return next(uCtx)
		}
	})

	// Recover from panics and give control to the HTTPError handler
	// so we reply http
	router.Use(echomiddleware.Recover())
	router.Use(prom.NewMiddleware("envvars_api"))
}
