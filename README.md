# Toolforge envvars API

API to manage environment variables (secrets and other config) for tools deployed in toolforge.

For more documentation about the Toolforge Envvars Service visit, [here](https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Envvars_Service)

## Development

Before you begin, you need to have golang and oapi-codegen installed.
To install and setup golang, follow the link https://go.dev/doc/install

After you've installed golang, use the command below to install oapi-codegen:
```bash
> make install-oapi-codegen
```

### Modifying the OpenAPI Definition

The core of the app is generated by `oapi-codegen` from the `openapi/v1.yaml` file.
If you modify the OpenAPI YAML definition, you need to regenerate the code:

```bash
> make gen-api
```

Then, you can manually modify the code under `cmd/main.go` to match the new
changes if necessary.

## Building the Docker Image

To build the Docker image, run:

```bash
> make image
```

This will create a `toolsbeta-harbor.wmcloud.org/toolforge/envvars-api:dev` image locally, which you can then re-tag and push if you want.

## Updating the Code After Changing the OpenAPI YAML File

After modifying the `openapi/v1.yaml` file, regenerate the API code:

```bash
> make gen-api
```

You might have to tweak `cmd/main.go` to add any new configuration, or `internal/api.go` to add or remove old handlers or change any if needed.

## Testing Locally

You can run tests with `make test`.
Check out `make help` for other useful make targets.

## Local Deployment

### Prerequisites

- Minikube
- Toolforge [Cert-manager](https://gitlab.wikimedia.org/repos/cloud/toolforge/cert-manager)
- Toolforge [API Gateway](https://gitlab.wikimedia.org/repos/cloud/toolforge/api-gateway)

### Running Outside Kubernetes

You will still need a Kubernetes cluster as it will create the envvars there, but you can run the server outside of it using your `$KUBECONFIG` Kubernetes configuration:

```bash
> OUT_OF_K8S_RUN=true go run cmd/main.go
```

### Running Inside Kubernetes

The application should work with both Podman/Docker and Kind/Minikube out of the box:

```bash
> make build-and-deploy-local
```

To only build the image, run:

```bash
> make image
```

To only rollout a built image, run:

```bash
> make rollout
```

To only deploy the chart changes, run:

```bash
> ./deploy.sh
```

## Accessing Locally

Start a tunnel to expose the API gateway with:

```bash
> minikube service api-gateway -n api-gateway
```

Then, you can make a curl request:

```bash
> curl --cert ~/.minikube/profiles/minikube/client.crt --key ~/.minikube/profiles/minikube/client.key --insecure 'https://192.168.49.2:30003/envvars/v1/healthz'
```
