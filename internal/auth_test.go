package internal

import (
	"testing"
)

var invalidSubjectFailMsg = "Expected an error for an invalid subject line."

func TestValidateUserFailsWhenInvalidSubject(t *testing.T) {
	_, err := ValidateUser("dummy invalid subject line")

	if err == nil {
		t.Error(invalidSubjectFailMsg)
	}
}

func TestValidateUserFailsWhenNoOrg(t *testing.T) {
	_, err := ValidateUser("CN=dummyuserwithoutorg")

	if err == nil {
		t.Error(invalidSubjectFailMsg)
	}
}

func TestValidateUserFailsWhenNoOrgIsNotToolforge(t *testing.T) {
	_, err := ValidateUser("CN=dummyuser,O=nottoolforge")

	if err == nil {
		t.Error(invalidSubjectFailMsg)
	}
}

func TestValidateUserReturnsUserWhenOrgIsToolforge(t *testing.T) {
	expectedUser := "myuser"
	gottenUser, err := ValidateUser("CN=myuser,O=toolforge")

	if err != nil {
		t.Errorf("Expected no error but got: %s", err)
	}

	if gottenUser != expectedUser {
		t.Errorf("I was expecting user '%s', but got '%s'", expectedUser, gottenUser)
	}
}
