package internal

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.wikimedia.org/repos/cloud/toolforge/envvars-api/gen"
	"k8s.io/client-go/kubernetes"
)

type UserContextKey string

type UserContext struct {
	echo.Context
	User string
}

type Config struct {
	MyNamespace string
}

type Clients struct {
	K8s kubernetes.Interface
}

type EnvvarsApi struct {
	Clients        Clients
	Config         Config
	MetricsHandler echo.HandlerFunc
}

// Extract the tool name from the context, currently is the same user that authenticated to the api
func getToolFromContext(ctx echo.Context) string {
	uCtx := ctx.(UserContext)
	return uCtx.User
}

func getNamespaceForTool(toolName string) string {
	return fmt.Sprintf("tool-%s", toolName)
}

// TODO: Dummy auth for now; implement real authn/authz when we have it
func checkToolnameAuthorization(toolNameFromPath string, toolNameFromContext string) error {
	if toolNameFromPath != toolNameFromContext {
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	return nil
}

// Handlers

func (api EnvvarsApi) Show(ctx echo.Context, id string) error {
	toolName := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolName)

	code, response := GetEnvvar(api, id, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) ShowWithToolname(ctx echo.Context, toolname string, id string) error {
	toolName := getToolFromContext(ctx)
	if err := checkToolnameAuthorization(toolname, toolName); err != nil {
		return err
	}
	toolNamespace := getNamespaceForTool(toolName)

	code, response := GetEnvvar(api, id, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) List(ctx echo.Context) error {
	toolName := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolName)

	code, response := GetEnvvars(api, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) ListWithToolname(ctx echo.Context, toolname string) error {
	toolName := getToolFromContext(ctx)
	if err := checkToolnameAuthorization(toolname, toolName); err != nil {
		return err
	}
	toolNamespace := getNamespaceForTool(toolName)

	code, response := GetEnvvars(api, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Create(ctx echo.Context) error {
	toolName := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolName)

	var newEnvvar gen.Envvar
	err := (&echo.DefaultBinder{}).BindBody(ctx, &newEnvvar)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, fmt.Sprintf("Bad parameters passed: %s", err))
	}

	code, response := CreateEnvvar(
		api,
		newEnvvar.Name,
		newEnvvar.Value,
		toolNamespace,
	)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) CreateWithToolname(ctx echo.Context, toolname string) error {
	toolName := getToolFromContext(ctx)
	if err := checkToolnameAuthorization(toolname, toolName); err != nil {
		return err
	}
	toolNamespace := getNamespaceForTool(toolName)

	var newEnvvar gen.Envvar
	err := (&echo.DefaultBinder{}).BindBody(ctx, &newEnvvar)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, fmt.Sprintf("Bad parameters passed: %s", err))
	}

	code, response := CreateEnvvar(
		api,
		newEnvvar.Name,
		newEnvvar.Value,
		toolNamespace,
	)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Delete(ctx echo.Context, id string) error {
	toolName := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolName)

	code, response := DeleteEnvvar(api, id, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) DeleteWithToolname(ctx echo.Context, toolname string, id string) error {
	toolName := getToolFromContext(ctx)
	if err := checkToolnameAuthorization(toolname, toolName); err != nil {
		return err
	}
	toolNamespace := getNamespaceForTool(toolName)

	code, response := DeleteEnvvar(api, id, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Quota(ctx echo.Context) error {
	toolName := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolName)

	code, response := GetQuota(api, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) QuotaWithToolname(ctx echo.Context, toolname string) error {
	toolName := getToolFromContext(ctx)
	if err := checkToolnameAuthorization(toolname, toolName); err != nil {
		return err
	}
	toolNamespace := getNamespaceForTool(toolName)

	code, response := GetQuota(api, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Healthcheck(ctx echo.Context) error {
	code, response, err := Healthcheck(&api)
	if err != nil {
		return err
	}
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Metrics(ctx echo.Context) error {
	return api.MetricsHandler(ctx)
}

func (api EnvvarsApi) Openapi(ctx echo.Context) error {
	swagger, err := gen.GetSwagger()
	if err != nil {
		return fmt.Errorf("error loading swagger spec\n: %s", err)
	}
	return ctx.JSON(http.StatusOK, swagger)
}
