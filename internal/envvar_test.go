package internal

import (
	"fmt"
	"net/http"
	"strings"
	"testing"

	"gitlab.wikimedia.org/repos/cloud/toolforge/envvars-api/gen"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	k8sFake "k8s.io/client-go/kubernetes/fake"
	k8sTesting "k8s.io/client-go/testing"
)

func getDummyApi(responses ...runtime.Object) EnvvarsApi {

	fakeK8s := k8sFake.NewSimpleClientset(responses...)
	return EnvvarsApi{
		Clients: Clients{
			K8s: fakeK8s,
		},
		Config: Config{
			MyNamespace: "app-namespace",
		},
	}
}

func intAddress(what int) *int {
	return &what
}

func envvarToString(envvar *gen.Envvar) string {
	return fmt.Sprintf("Name: %s, Value: %s", envvar.Name, envvar.Value)
}

func assertEqualEnvvars(t *testing.T, envvar1 *gen.Envvar, envvar2 *gen.Envvar) {
	if envvar1.Name != envvar2.Name || envvar1.Value != envvar2.Value {
		t.Fatalf("Got different envvars, left: %s, right: %s", envvarToString(envvar1), envvarToString(envvar2))
	}
}

func TestCreateReturnsErrorIfEnvvarNameNotUppercase(t *testing.T) {
	api := getDummyApi()

	code, _ := CreateEnvvar(
		api,
		"not_uppercase",
		"dummyvalue",
		"dummy-namespace",
	)

	if code != http.StatusInternalServerError {
		t.Fatalf("I was expecting %d response, got: %d", http.StatusInternalServerError, code)
	}
}

func TestCreateReturnsErrorIfEnvvarNameHasNonValidChars(t *testing.T) {
	api := getDummyApi()

	code, _ := CreateEnvvar(
		api,
		"badname",
		"dummyvalue",
		"dummy-namespace",
	)

	if code != http.StatusInternalServerError {
		t.Fatalf("I was expecting %d response, got: %d", http.StatusInternalServerError, code)
	}
}

func TestCreateReturnsTheNewEnvvar(t *testing.T) {
	api := getDummyApi()
	expectedEnvvar := gen.Envvar{
		Name:  "DUMMY1",
		Value: "dummyvalue",
	}

	code, response := CreateEnvvar(
		api,
		expectedEnvvar.Name,
		expectedEnvvar.Value,
		"dummy-namespace",
	)

	if code != http.StatusOK {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to unmarshal): %v", http.StatusOK, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusOK, code, responseValue)
	}

	gottenResponse, ok := response.(gen.GetResponse)
	if !ok {
		t.Fatalf("Unable to unmarshall response %v", response)
	}

	assertEqualEnvvars(t, gottenResponse.Envvar, &expectedEnvvar)
}

func TestListGetsAllEnvvars(t *testing.T) {
	expectedEnvvars := []gen.Envvar{
		{
			Name:  "DUMMY1",
			Value: "dummyvalue1",
		},
		{
			Name:  "DUMMY2",
			Value: "dummyvalue2",
		},
	}
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedEnvvars[0].Name)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "toolforge",
						},
					},
					Data: map[string][]byte{
						expectedEnvvars[0].Name: []byte(expectedEnvvars[0].Value),
					},
				},
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedEnvvars[1].Name)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "toolforge",
						},
					},
					Data: map[string][]byte{
						expectedEnvvars[1].Name: []byte(expectedEnvvars[1].Value),
					},
				},
			},
		},
	)

	code, response := GetEnvvars(
		api,
		"dummy-namespace",
	)

	if code != http.StatusOK {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to unmarshal): %v", http.StatusOK, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusOK, code, (*responseValue.Error)[0])
	}

	gottenResponse, ok := response.(gen.ListResponse)
	gottenEnvvars := *gottenResponse.Envvars
	if !ok {
		t.Fatalf("Unable to unmarshall response: %v", response)
	}

	for index, expectedEnvvar := range expectedEnvvars {
		assertEqualEnvvars(t, &expectedEnvvar, &gottenEnvvars[index])
	}
}

func TestGetGetsEnvvar(t *testing.T) {
	expectedEnvvar := gen.Envvar{
		Name:  "DUMMY1",
		Value: "dummyvalue1",
	}
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedEnvvar.Name)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "toolforge",
						},
					},
					Data: map[string][]byte{
						expectedEnvvar.Name: []byte(expectedEnvvar.Value),
					},
				},
			},
		},
	)
	code, response := GetEnvvar(
		api,
		"DUMMY1",
		"dummy-namespace",
	)
	if code != http.StatusOK {
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusOK, code, response)
	}

	gottenResponse, ok := response.(gen.GetResponse)
	if !ok {
		t.Fatalf("Unable to unmarshall response: %s", response)
	}
	assertEqualEnvvars(t, &expectedEnvvar, gottenResponse.Envvar)
}

func TestGetSkipsWrongLabel(t *testing.T) {
	expectedEnvvar := gen.Envvar{
		Name:  "DUMMY1",
		Value: "dummyvalue1",
	}
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{
				// Can't add two secrets with the same name, so the test is to see if it returns none
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedEnvvar.Name)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "not toolforge should be skipped",
						},
					},
					Data: map[string][]byte{
						expectedEnvvar.Name: []byte(expectedEnvvar.Value),
					},
				},
			},
		},
	)
	code, response := GetEnvvar(
		api,
		"DUMMY1",
		"dummy-namespace",
	)

	if code != http.StatusNotFound {
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusNotFound, code, response)
	}
}

func TestGetSkipsWrongName(t *testing.T) {
	expectedEnvvar := gen.Envvar{
		Name:  "DUMMY1",
		Value: "dummyvalue1",
	}
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "wrong-prefix-baddummy",
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "toolforge",
						},
					},
					Data: map[string][]byte{
						expectedEnvvar.Name: []byte(expectedEnvvar.Value),
					},
				},
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedEnvvar.Name)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "toolforge",
						},
					},
					Data: map[string][]byte{
						expectedEnvvar.Name: []byte(expectedEnvvar.Value),
					},
				},
			},
		},
	)
	code, response := GetEnvvar(
		api,
		"DUMMY1",
		"dummy-namespace",
	)

	if code != http.StatusOK {
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusOK, code, response)
	}

	gottenResponse, ok := response.(gen.GetResponse)
	if !ok {
		t.Fatalf("Unable to unmarshall response: %v", response)
	}

	assertEqualEnvvars(t, gottenResponse.Envvar, &expectedEnvvar)
}
func TestDeleteReturnsEnvvarIfEnvvarExists(t *testing.T) {
	expectedEnvvar := gen.Envvar{
		Name:  "DUMMY1",
		Value: "dummyvalue1",
	}
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedEnvvar.Name)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "toolforge",
						},
					},
					Data: map[string][]byte{
						expectedEnvvar.Name: []byte(expectedEnvvar.Value),
					},
				},
			},
		},
	)

	code, response := DeleteEnvvar(
		api,
		"DUMMY1",
		"dummy-namespace",
	)

	if code != http.StatusOK {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to understand the response): %v", http.StatusOK, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusOK, code, responseValue)
	}

	gottenResponse, ok := response.(gen.GetResponse)
	if !ok {
		t.Fatalf("I was expecting %d response, got %d (and was unable to understand the response): %v", http.StatusOK, code, response)
	}

	assertEqualEnvvars(t, gottenResponse.Envvar, &expectedEnvvar)
}

func TestDeleteSkipsWrongLabel(t *testing.T) {
	expectedName := "DUMMY1"
	expectedValue := "dummyvalue1"
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name:      fmt.Sprintf("%s%s", envvarPrefix, strings.ToLower(expectedName)),
						Namespace: "dummy-namespace",
						Labels: map[string]string{
							"app": "not toolforge",
						},
					},
					Data: map[string][]byte{
						expectedName: []byte(expectedValue),
					},
				},
			},
		},
	)

	code, response := DeleteEnvvar(
		api,
		"DUMMY1",
		"dummy-namespace",
	)

	if code != http.StatusNotFound {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to understand the response): %v", http.StatusNotFound, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusNotFound, code, responseValue)
	}
}

func TestGetQuotaReturnsErrorIfSecretsListReturnsError(t *testing.T) {
	api := getDummyApi()
	fakeClientset := api.Clients.K8s.(*k8sFake.Clientset)
	fakeClientset.PrependReactor("list", "secrets", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, nil, fmt.Errorf("Dummy error")
	})

	code, response := GetQuota(api, "dummy-namespace")

	if code != http.StatusInternalServerError {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to understand the response): %v", http.StatusInternalServerError, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusInternalServerError, code, responseValue)
	}
}

func TestGetQuotaReturnsErrorIfResourceQuotasListReturnsError(t *testing.T) {
	api := getDummyApi()
	fakeClientset := api.Clients.K8s.(*k8sFake.Clientset)
	fakeClientset.PrependReactor("list", "resourcequotas", func(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {
		return true, nil, fmt.Errorf("Dummy error")
	})

	code, response := GetQuota(api, "dummy-namespace")

	if code != http.StatusInternalServerError {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to understand the response): %v", http.StatusInternalServerError, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusInternalServerError, code, responseValue)
	}
}

func TestGetQuotaReturnsErrorIfResourceQuotasListIsNotListOfOneItemOnly(t *testing.T) {
	api := getDummyApi(
		&v1.SecretList{
			Items: []v1.Secret{},
		},
		&v1.ResourceQuotaList{
			Items: []v1.ResourceQuota{},
		},
	)

	code, response := GetQuota(api, "dummy-namespace")

	if code != http.StatusInternalServerError {
		responseValue, ok := response.(gen.ResponseMessages)
		if !ok {
			t.Fatalf("I was expecting %d response, got %d (and was unable to understand the response): %v", http.StatusInternalServerError, code, response)
		}
		t.Fatalf("I was expecting %d response, got %d: %v", http.StatusInternalServerError, code, responseValue)
	}
}

func TestGetQuotaReturnsExpectedEnvvarsQuotas(t *testing.T) {
	namespaceName := "dummy-tool"
	resourceQuotas := v1.ResourceQuotaList{
		Items: []v1.ResourceQuota{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      namespaceName,
					Namespace: namespaceName,
				},
				Spec: v1.ResourceQuotaSpec{
					Hard: v1.ResourceList{
						v1.ResourceSecrets: *resource.NewQuantity(4, resource.DecimalSI),
					},
				},
			},
		},
	}

	testCases := []struct {
		desc                 string
		envvarsEntries       []gen.Envvar
		expectedEnvvarsQuota gen.EnvvarsQuota
	}{
		{
			desc:           "0 envvars 0 others",
			envvarsEntries: []gen.Envvar{},
			expectedEnvvarsQuota: gen.EnvvarsQuota{
				Quota: intAddress(4),
				Used:  intAddress(0),
			},
		},
		{
			desc: "0 envvars 4 others",
			envvarsEntries: []gen.Envvar{
				{
					Name:  "DUMMY1",
					Value: "dummyvalue1",
				},
				{
					Name:  "DUMMY2",
					Value: "dummyvalue2",
				},
				{
					Name:  "DUMMY3",
					Value: "dummyvalue3",
				},
				{
					Name:  "DUMMY4",
					Value: "dummyvalue4",
				},
			},
			expectedEnvvarsQuota: gen.EnvvarsQuota{
				Quota: intAddress(0),
				Used:  intAddress(0),
			},
		},
		{
			desc: "0 envvars 3 others",
			envvarsEntries: []gen.Envvar{
				{
					Name:  "DUMMY1",
					Value: "dummyvalue1",
				},
				{
					Name:  "DUMMY2",
					Value: "dummyvalue2",
				},
				{
					Name:  "DUMMY3",
					Value: "dummyvalue3",
				},
			},
			expectedEnvvarsQuota: gen.EnvvarsQuota{
				Quota: intAddress(1),
				Used:  intAddress(0),
			},
		},
		{
			desc: "2 envvars 2 others",
			envvarsEntries: []gen.Envvar{
				{
					Name:  "toolforge.envvar.v1.dummy1",
					Value: "dummyvalue1",
				},
				{
					Name:  "toolforge.envvar.v1.dummy2",
					Value: "dummyvalue2",
				},
				{
					Name:  "DUMMY3",
					Value: "dummyvalue3",
				},
				{
					Name:  "DUMMY4",
					Value: "dummyvalue4",
				},
			},
			expectedEnvvarsQuota: gen.EnvvarsQuota{
				Quota: intAddress(2),
				Used:  intAddress(2),
			},
		},
		{
			desc: "3 envvars 0 others",
			envvarsEntries: []gen.Envvar{
				{
					Name:  "toolforge.envvar.v1.dummy1",
					Value: "dummyvalue1",
				},
				{
					Name:  "toolforge.envvar.v1.dummy2",
					Value: "dummyvalue2",
				},
				{
					Name:  "toolforge.envvar.v1.dummy3",
					Value: "dummyvalue3",
				},
			},
			expectedEnvvarsQuota: gen.EnvvarsQuota{
				Quota: intAddress(4),
				Used:  intAddress(3),
			},
		},
		{
			desc: "4 envvars 0 others",
			envvarsEntries: []gen.Envvar{
				{
					Name:  "toolforge.envvar.v1.dummy1",
					Value: "dummyvalue1",
				},
				{
					Name:  "toolforge.envvar.v1.dummy2",
					Value: "dummyvalue2",
				},
				{
					Name:  "toolforge.envvar.v1.dummy3",
					Value: "dummyvalue3",
				},
				{
					Name:  "toolforge.envvar.v1.dummy4",
					Value: "dummyvalue4",
				},
			},
			expectedEnvvarsQuota: gen.EnvvarsQuota{
				Quota: intAddress(4),
				Used:  intAddress(4),
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.desc, func(t *testing.T) {
			secrets := []v1.Secret{}

			for _, envvar := range testCase.envvarsEntries {
				appLabel := "toolforge"
				if !strings.HasPrefix(envvar.Name, "toolforge.envvar.v1.") {
					appLabel = "not toolforge"
				}
				EnvvarDataKey := strings.ToUpper(strings.TrimPrefix(envvar.Name, "toolforge.envvar.v1."))

				secrets = append(secrets, v1.Secret{
					ObjectMeta: metav1.ObjectMeta{
						Name:      envvar.Name,
						Namespace: namespaceName,
						Labels: map[string]string{
							"app": appLabel,
						},
					},
					Data: map[string][]byte{
						EnvvarDataKey: []byte(envvar.Value),
					},
				})
			}

			api := getDummyApi(
				&resourceQuotas,
				&v1.SecretList{
					Items: secrets,
				},
			)

			code, response := GetQuota(api, namespaceName)

			if code != http.StatusOK {
				t.Fatalf("I was expecting %d response, got %d: %v", http.StatusOK, code, response)
			}

			gottenResponse, ok := response.(gen.QuotaResponse)
			gottenEnvvarsQuota := gottenResponse.Quota
			if !ok {
				t.Fatalf("Unable to unmarshall response: %v", response)
			}

			if *gottenEnvvarsQuota.Quota != *testCase.expectedEnvvarsQuota.Quota {
				t.Fatalf("I was expecting quota to be %d, got %d", *testCase.expectedEnvvarsQuota.Quota, *gottenEnvvarsQuota.Quota)
			}

			if *gottenEnvvarsQuota.Used != *testCase.expectedEnvvarsQuota.Used {
				t.Fatalf("I was expecting used to be %d, got %d", *testCase.expectedEnvvarsQuota.Used, *gottenEnvvarsQuota.Used)
			}
		})
	}
}
